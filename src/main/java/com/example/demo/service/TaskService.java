package com.example.demo.service;

import com.example.demo.model.Task;

import java.util.List;

public interface TaskService {

    List<Task> getAll();
    Task getById(long id);

    Task update(Task newTask);

    void save(Task task);

    void deleteById(long id);

}
