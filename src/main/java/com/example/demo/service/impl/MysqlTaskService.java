package com.example.demo.service.impl;

import com.example.demo.model.Task;
import com.example.demo.repository.TaskRepository;
import com.example.demo.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class MysqlTaskService implements TaskService {

    private final TaskRepository taskRepository;

    @Autowired
    public MysqlTaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> getAll() {
        return taskRepository.findAll();
    }

    @Override
    public Task getById(long id) {
        return taskRepository.findById(id).orElse(null);
    }

    @Override
    public Task update(Task newTask) {
        taskRepository.save(newTask);
        return newTask;
    }

    @Override
    public void save(Task task) {
        task.setCreatedDate(LocalDateTime.now());
        taskRepository.save(task);
    }

    @Override
    public void deleteById(long id) {
        taskRepository.deleteById(id);
    }
}
