package com.example.demo.controller;

import com.example.demo.model.Task;
import com.example.demo.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/tasks")
public class TaskController {

    final
    TaskService taskService;

    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping
    public ResponseEntity<List<Task>> getAll() {
        return new ResponseEntity<>(taskService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Task> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(taskService.getById(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Task> create(@RequestBody Task task) {
        taskService.save(task);
        return new ResponseEntity<>(task, HttpStatus.CREATED);
    }

    @PatchMapping
    public ResponseEntity<Task> update(@RequestBody Task task) {
        taskService.update(task);
        return new ResponseEntity<>(task, HttpStatus.OK);
    }

    @PostMapping("/test/{id}")
    public ResponseEntity<String> test(@PathVariable("id") long id,
                                       @RequestParam("name") String name,
                                       @RequestBody Task task) {

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(id).append("\n")
                .append(name).append("\n")
                .append(task);

        return new ResponseEntity<>(stringBuilder.toString(), HttpStatus.OK);
    }

}
